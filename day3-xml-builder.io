OperatorTable addAssignOperator(":", "xmlAtPut")

curlyBrackets := method(
    r := Map clone
    call message arguments foreach(arg,
        r doMessage(arg)
    )
    r
)

Map xmlAtPut := method(where, what,
    self atPut(
        where asMutable removePrefix("\"") removeSuffix("\""),
        what asMutable prependSeq("\"") appendSeq("\"")
    )
)


Builder := Object clone
Builder indent := 0

Builder forward := method(
    indent repeat(write("    "))
    write("<", call message name)

    m := call message arguments at(0)
    if(m name == "curlyBrackets",
        m := Object doMessage(m)
    )
    if(m type == "Map",
        m foreach(key, value,
            write(" ", key, "=", value)
        )
    )
    writeln(">")

    indent = indent + 1
    call message arguments foreach(arg,
        content := self doMessage(arg)

        if(content type == "Sequence",
            indent repeat(write("    "))
            writeln(content)
        )
    )
    indent = indent - 1

    indent repeat(write("    "))
    writeln("</", call message name, ">")
)


Builder languages(
    io({"type": "prototype", "runs": "interpreted"}, "Io"),
    ruby({"type": "object oriented", "runs": "interpreted"}, "Ruby"),
    java({"type": "object oriented", "runs": "compiled"}, "Java")
)
