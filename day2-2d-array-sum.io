List mySum := method(
    sum := 0
    flatten foreach(element,
        sum = sum + element
    )

    return sum
)

a2dList := list(
    list(1, 2, 3, 4),
    list(9, 8, 7, 6),
    list(4, 5, 6, 7),
    list(6, 5, 4, 3)
)

a2dList mySum println
