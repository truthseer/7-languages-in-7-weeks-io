fib := method(n,
    if(n < 1, Exception raise("The argument must be 1 or greater"))

    current := 0
    next := 1

    for(i, 1, n - 1,
        temp := next
        next := current + next
        current = temp
    )

    return current
)

fib(1) println
fib(2) println
fib(3) println
fib(4) println
fib(5) println
fib(9) println
fib(10) println
fib(11) println
fib(20) println
