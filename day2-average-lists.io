List myAverage := method(
    sum := 0

    foreach(element,
        if(element type != "Number", Exception raise(
            "This list doesn't only have numbers")
        )

        sum := sum + element
    )

    return sum / size
)

array := list(1, 3, 4, 1, 7, 8, 24)

array myAverage println
