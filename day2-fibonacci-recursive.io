fib := method(n,
    if(n < 1, Exception raise("The argument must be 1 or greater"))

    if(n == 1, return 0)
    if(n == 2, return 1)

    return fib(n - 1) + fib(n - 2)
)

fib(1) println
fib(2) println
fib(3) println
fib(4) println
fib(5) println
fib(9) println
fib(10) println
fib(11) println
fib(20) println
