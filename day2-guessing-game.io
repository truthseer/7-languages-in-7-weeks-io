stdin := File standardInput

get_int := method(prompt,
    if(prompt isNil, prompt := "\nNumber: ")

    prompt print
    return stdin readLine strip asNumber
)

"Welcome to the guessing game!" println
answer := Random value(0, 100) round
guess := nil
guesses := 0

while(10 > guesses,
    prev_guess := guess
    guess := get_int("\nYour guess: ")
    guesses := guesses + 1

    if(guess != answer,
        if(prev_guess != nil,
            ((answer - prev_guess) abs > (answer - guess) abs) ifTrue(
                clue := "You're getting hotter!"
            ) ifFalse(clue := "You're getting colder guff ;)")

            if(guesses != 10,
                (clue .. " ...") println,
                "You ran out of guesses.  Please try again later." println
		guesses := guesses + 1 # makes it so Correct! isn't printed
            ),
            "Wrong!  Try again ..." println
        ),
        break
    )
)

if(10 >= guesses, "Correct!  " print)
("The answer was " .. answer .. ".") println
