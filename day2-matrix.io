Matrix := Object clone

Matrix println := method(
    "list(" println
    grid foreach(element,
        "    " print
        element println)
    ")" println
)

Matrix dim := method(x, y,
    m := self clone
    m size := list(x, y)
    m grid := list()

    for(i, 1, y,
        m grid append(list setSize(x))
    )

    return m
)

Matrix get := method(x, y,
    return grid at(y) at(x)
)

Matrix set := method(x, y, value,
    (value type == "Number") ifFalse(
        value isNil ifFalse(
            Exception raise("A matrix can only have numbers")
        )
    )

    grid at(y) atPut(x, value)
)

Matrix transpose := method(
    if(size at(0) != size at(1),
        Exception raise("This is not a square matrix")
    )

    nm := Matrix clone
    nm size := size clone
    nm grid := list()
    grid foreach(element, nm grid append(element clone))

    for(y, 0, nm size at(1) - 1,
        for(x, y + 1, nm size at(0) - 1,
            temp := nm get(x, y)
            nm set(x, y, nm get(y, x))
            nm set(y, x, temp)
        )
    )

    return nm
)


FileMatrix := Object clone

FileMatrix write := method(filename, matrix,
    file := File openForUpdating(filename .. ".matrix")

    matrix grid foreach(row,
        row foreach(col,
            file write(col asString, " ,;, ")
        )

        file write("\n")
    )

    file close
)

FileMatrix read := method(filename,
    file := File openForReading(filename .. ".matrix")
    grid := list()

    file foreachLine(line,
        grid append(line split(" ,;, ") map(element,
            if(element == "nil", nil, element asNumber)))
    )

    file close

    nm := Matrix clone
    nm size := list(grid first size, grid size)
    nm grid := grid

    return nm
)


x := 1
y := 3

matrix := Matrix dim(4, 4)
matrix set(x, y, 934)
matrix get(x, y) println

new_matrix := matrix transpose
if(new_matrix get(y, x) == matrix get(x, y),
    "it's done" println,
    "it isn't done" println
)

FileMatrix write("data", matrix)
read_matrix := FileMatrix read("data")

if(read_matrix grid == matrix grid,
    "it's done" println,
    "it isn't done" println
)
