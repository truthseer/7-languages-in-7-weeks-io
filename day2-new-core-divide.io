Number olddiv := Number getSlot("/")

Number / := method(num,
    if(num == 0, return 0)
    return olddiv(num)
)

(-45 / 0) println
(0 / 0) println
(1 / 0) println
(23 / 0) println
(12 / 5) println
(10 / 2) println
